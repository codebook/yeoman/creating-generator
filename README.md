# How to Create a Yeoman Generator


Use Yeoman Generator-Generator:

    npm i -g yo generator-generator


Update and modify your generator code.


Install locally and test:

    cd generator-my-generator
    npm link


Remove the local package:

    npm uninstll --global generator-my-generator


Publish the package to the npm repo:

    tar czf generator-my-generator.tgz generator-my-generator
    npm publish generator-my-generator.tgz --access public





## References

* [WRITING YOUR OWN YEOMAN GENERATOR](http://yeoman.io/authoring/)
* [Generate a Yeoman generator](https://github.com/yeoman/generator-generator)
* [npm-publish](https://docs.npmjs.com/cli/publish)


